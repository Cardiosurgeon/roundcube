<?php
$config['oauth_provider'] = 'generic';
$config['oauth_provider_name'] = 'Craynic';
$config['oauth_client_id'] = $_ENV['ROUNDCUBE_OAUTH2_CLIENT_ID'];
$config['oauth_client_secret'] = $_ENV['ROUNDCUBE_OAUTH2_CLIENT_SECRET'];
$config['oauth_auth_uri'] = 'https://localhost:8443/oauth2/authorize';
$config['oauth_token_uri'] = 'https://host.docker.internal:8443/api/oauth2/token';
$config['oauth_identity_uri'] = 'https://host.docker.internal:8443/api/oauth2/introspection';
$config['oauth_scope'] = 'email';
$config['oauth_auth_parameters'] = [];
$config['oauth_identity_fields'] = ['username'];
$config['oauth_login_redirect'] = true;
$config['oauth_verify_peer'] = false;
