<?php

/** @noinspection PhpUndefinedConstantInspection */
/** @noinspection PhpUnused */

class rcube_craynic_password
{
    function save($oldPassword, $newPassword, $username)
    {
        try {
            $response = $this->makeCall(
                sprintf('mailbox/email/%s', urlencode($username)),
                $username,
                $oldPassword,
                null,
                [
                    CURLOPT_HTTPHEADER => [
                        'Content-Type: application/json'
                    ],
                ],
                [200],
            );

            $decoded = json_decode($response);
            if (!is_object($decoded)) {
                throw new RuntimeException('Invalid syntax for the call to get user ID.', PASSWORD_CONNECT_ERROR);
            }

            // get the user ID
            $userId = (int) $decoded->mailbox_id;

            $this->makeCall(
                sprintf('mailbox/%d/password', $userId),
                $username,
                $oldPassword,
                ['password' => $newPassword],
            );

            /** @noinspection PhpUndefinedConstantInspection */
            return PASSWORD_SUCCESS;
        } catch (Throwable $exception) {
            /** @noinspection PhpUndefinedConstantInspection */
            switch ($exception->getCode()) {
                case PASSWORD_CRYPT_ERROR:
                case PASSWORD_ERROR:
                case PASSWORD_CONNECT_ERROR:
                case PASSWORD_IN_HISTORY:
                case PASSWORD_CONSTRAINT_VIOLATION:
                case PASSWORD_COMPARE_CURRENT:
                case PASSWORD_COMPARE_NEW:
                    return $exception->getCode();

                default:
                    return PASSWORD_ERROR;
            }
        }
    }

    private function makeCall(
        string $apiUri,
        string $username,
        string $oldPassword,
        array $postFields = null,
        array $extraOpts = [],
        array $expectedResponseCodes = [200, 204]
    ) {
        /** @noinspection PhpUndefinedClassInspection */
        $rcmail = rcmail::get_instance();
        $apiEndpoint = $rcmail->config->get('password_craynic_api_endpoint');
        $pathToClientCert = $rcmail->config->get('password_craynic_api_certfile');

        if (empty($apiEndpoint)) {
            throw new RuntimeException('API endpoint not defined.', PASSWORD_ERROR);
        }

        $options = [
            CURLOPT_URL => sprintf('%s/%s', $apiEndpoint, $apiUri),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_USERPWD => sprintf('%s:%s', $username, $oldPassword),
            CURLOPT_HTTPHEADER => [
                'Cache-Control: no-cache',
            ],
        ];

        if (!empty($pathToClientCert)) {
            $options[CURLOPT_SSLCERT] = $pathToClientCert;
            $options[CURLOPT_SSLCERTTYPE] = strtoupper(pathinfo($pathToClientCert, PATHINFO_EXTENSION));
        }

        foreach ($extraOpts as $name => $value) {
            $options[$name] = is_array($value) && is_array($options[$name] ?? null)
                ? array_merge($options[$name], $value)
                : $value;
        }

        if ($postFields !== null) {
            $options[CURLOPT_CUSTOMREQUEST] = is_array($postFields) ? 'POST' : 'GET';
            $options[CURLOPT_POSTFIELDS] = $postFields;
        }

        // Call GET to fetch values from server
        $curl = curl_init();
        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if ($err || !in_array($info['http_code'] ?? null, $expectedResponseCodes)) {
            throw new RuntimeException($err, PASSWORD_CONNECT_ERROR);
        }

        return $response;
    }
}
