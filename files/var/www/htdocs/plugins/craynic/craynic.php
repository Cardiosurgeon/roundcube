<?php

/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedClassInspection */

/**
 * @method add_hook(string $event, callable $callback)
 * @method load_config(string $fname = 'config.inc.php')
 */
final class craynic extends rcube_plugin
{
    private ?rcmail $rc = null;

    public function init(): void
    {
        $this->rc = rcmail::get_instance();
        $this->load_config();

        $this->add_hook('logout_after', [$this, 'logout_after']);
    }

    public function logout_after(): void
    {
        $redirectUrl = (string) $this->rc->config->get('craynic_logout_redirect_url');

        if ($redirectUrl !== '') {
            header(sprintf('Location: %s', $redirectUrl), true, 303);
        }
    }
}
