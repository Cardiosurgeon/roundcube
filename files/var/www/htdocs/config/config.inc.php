<?php

declare(strict_types=1);

$config['plugins'] = [
    'craynic',
    'emoticons',
    'managesieve',
    'markasjunk',
    'password',
    'vcard_attachments',
    'zipdownload',
];

$config['skin'] = 'elastic';

$config['log_driver'] = 'stdout';
$config['log_logins'] = true;

$config['zipdownload_selection'] = true;

$config['imap_vendor'] = 'dovecot';
$config['imap_log_session'] = true;
$config['imap_cache'] = 'db';

$config['session_lifetime'] = 60;

$config['email_dns_check'] = true;

$config['drafts_mbox'] = 'Drafts';
$config['junk_mbox'] = 'Junk';
$config['sent_mbox'] = 'Sent';
$config['trash_mbox'] = 'Trash';
$config['create_default_folders'] = true;

$config['check_all_folders'] = true;

$config['htmleditor'] = 3;

$config['reply_all_mode'] = 1;
$config['reply_mode'] = 1;

$config['date_format'] = 'j.n.Y';
$config['date_long'] = 'j.n.Y H:i';

$config['enable_spellcheck'] = true;
$config['spellcheck_dictionary'] = true;
$config['spellcheck_engine'] = 'pspell';

$config['default_charset'] = 'UTF-8';

// plugins
$config['password_confirm_current'] = true;
$config['password_force_save'] = true;
$config['password_minimum_length'] = 8;
$config['password_minimum_score'] = 1;
$config['password_log'] = true;
$config['password_algorithm'] = 'md5-crypt';
$config['password_driver'] = 'craynic';

$config['managesieve_host'] = '%h';
$config['managesieve_usetls'] = true;
$config['managesieve_vacation'] = 1;
$config['managesieve_forward'] = 1;

$config['smtp_xclient_login'] = true;
$config['smtp_xclient_addr'] = true;

$config['markasjunk_read_spam'] = true;
$config['markasjunk_unread_ham'] = true;
$config['markasjunk_move_spam'] = true;
$config['markasjunk_move_ham'] = true;
