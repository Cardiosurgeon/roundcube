stages:
  - init
  - test
  - build-image
  - test-image
  - release

include:
  - template: Security/Container-Scanning.gitlab-ci.yml

variables:
  CONTAINER_IMAGE: registry.gitlab.com/$CI_PROJECT_PATH

.rule-new-version-tag: &rule-new-version-tag
  - if: '$CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+(-b\d+)?$/'

init-environment:
  stage: init
  script:
    - VERSION_PATCH=${CI_COMMIT_TAG#v}
    - VERSION_PATCH=${VERSION_PATCH%%-b*}
    - VERSION_MINOR=${VERSION_PATCH%.*}
    - VERSION_MAJOR=${VERSION_MINOR%.*}
    - echo "VERSION_PATCH=$VERSION_PATCH" >> build.env
    - echo "VERSION_MINOR=$VERSION_MINOR" >> build.env
    - echo "VERSION_MAJOR=$VERSION_MAJOR" >> build.env
  artifacts:
    reports:
      dotenv: build.env
  rules:
    - *rule-new-version-tag

lint-dockerfile:
  image: hadolint/hadolint:latest-debian
  stage: test
  script:
    - hadolint Dockerfile
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push"'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_PIPELINE_SOURCE == "web"'

.build-image:template:
  image: docker:latest
  stage: build-image
  services:
    - docker:dind
  needs:
    - lint-dockerfile
    - job: init-environment
      optional: true
  before_script:
    - chmod -R go-w .
    - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" registry.gitlab.com

build-image:master:
  extends: .build-image:template
  script:
    - docker build --no-cache --tag "$CONTAINER_IMAGE:latest" .
    - docker push -a "$CONTAINER_IMAGE"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: 'never'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

build-image:version:
  extends: .build-image:template
  dependencies:
    - init-environment
  script:
    - docker pull "$CONTAINER_IMAGE:latest" || true
    - docker build --cache-from "$CONTAINER_IMAGE:latest" --tag "$CONTAINER_IMAGE:$VERSION_PATCH" .
    - docker tag "$CONTAINER_IMAGE:$VERSION_PATCH" "$CONTAINER_IMAGE:$VERSION_MINOR"
    - docker tag "$CONTAINER_IMAGE:$VERSION_PATCH" "$CONTAINER_IMAGE:$VERSION_MAJOR"
    - docker push -a "$CONTAINER_IMAGE"
  rules:
    - *rule-new-version-tag

mark-stable:
  image: docker:latest
  stage: build-image
  services:
    - docker:dind
  needs:
    - init-environment
    - build-image:version
  before_script:
    - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" registry.gitlab.com
  script:
    - docker pull "$CONTAINER_IMAGE:$VERSION_PATCH"
    - docker tag "$CONTAINER_IMAGE:$VERSION_PATCH" "$CONTAINER_IMAGE:stable"
    - docker push -a "$CONTAINER_IMAGE"
  rules:
    - *rule-new-version-tag

container_scanning:
  stage: test-image
  allow_failure: false
  variables:
    GIT_STRATEGY: fetch
    DOCKER_IMAGE: $CONTAINER_IMAGE:latest
  needs:
    - job: build-image:master
      optional: true
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

container_scanning_stable:
  extends: container_scanning
  variables:
    DOCKER_IMAGE: $CONTAINER_IMAGE:stable
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'

container_scanning_version:
  extends: container_scanning
  dependencies:
    - init-environment
  needs:
    - init-environment
    - build-image:version
  variables:
    DOCKER_IMAGE: $CONTAINER_IMAGE:$VERSION_PATCH
  rules:
    - *rule-new-version-tag

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  allow_failure: true
  rules:
    - *rule-new-version-tag
  before_script:
    - apk add git
  script:
    - git tag -l --format='%(contents)' "$CI_COMMIT_TAG" > ./__gitlab-release-tag_message.txt
  release:
    name: '$CI_COMMIT_TAG'
    description: './__gitlab-release-tag_message.txt'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'